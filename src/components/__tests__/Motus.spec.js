import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import Motus from '../Motus.vue'
import MotusWord from '../MotusWord.vue'

describe('Motus', () => {
    it("should display all required elements before any proposition is made (1 pts)", () => {
        let wrapper = mount(Motus,{props : {word : "motus"}});
        expect(wrapper.findAllComponents(MotusWord)).toHaveLength(1);
        expect(wrapper.find("button").exists()).toBe(true);
        expect(wrapper.find('input[type="text"]').exists()).toBe(true);
    });

    it("should have an empty input initially (0.5 pts)", () => {
        let wrapper = mount(Motus,{props : {word : "motus"}});
        expect(wrapper.find('input[type="text"]').exists()).toBe(true);
        expect(wrapper.find('input[type="text"]').element.value).toBe("");
    });

    it("should have an input limited by the size of the provided word (0.5 pts)", () => {
        let wrapper = mount(Motus,{props : {word : "motus"}});
        expect(wrapper.find('input[type="text"]').exists()).toBe(true);
        expect(wrapper.find('input[type="text"]').attributes()["maxlength"]).toBe("5");
        let wrapper2 = mount(Motus,{props : {word : "casque"}});
        expect(wrapper2.find('input[type="text"]').exists()).toBe(true);
        expect(wrapper2.find('input[type="text"]').attributes()["maxlength"]).toBe("6");
    });

    it("should update the unrevealed word while typing in the input (1 pts)", async () => {
        let wrapper = mount(Motus,{props : {word : "motus"}});
        await wrapper.get('input[type="text"]').setValue("ma");
        let attempts = wrapper.findAllComponents(MotusWord);
        expect(attempts.length).toBeGreaterThanOrEqual(1);
        expect(attempts[attempts.length - 1].props().proposition).toBe("ma");
    });

    it("should add a revealed word to the list when validated (1 pts)", async () => {
        let wrapper = mount(Motus,{props : {word : "motus"}});
        await testAttempt(wrapper,"matou",1);
    });

    it("should add as many revealed words as failed attempts are made (1 pts)", async () => {
        let wrapper = mount(Motus,{props : {word : "motus"}});
        await testAttempt(wrapper,"matou",1);
        await testAttempt(wrapper,"tatou",2);
        await testAttempt(wrapper,"tortue",3);
        expect(wrapper.findAllComponents(MotusWord)).toHaveLength(4);
    });

    it("should reset input when validated (0.5 pts)", async () => {
        let wrapper = mount(Motus,{props : {word : "fleuri"}});
        await attempt(wrapper,"elargi");
        expect(wrapper.get("input").element.value).toBe("");
    });

    it("should not do anything at validation if input is empty (0.5 pts)", async () => {
        let wrapper = mount(Motus,{props : {word : "fleuri"}});
        await attempt(wrapper,"");
        expect(wrapper.findAllComponents(MotusWord)).toHaveLength(1);
        expect(wrapper.findAllComponents(MotusWord)[0].props().proposition).toBe("");
        expect(wrapper.findAllComponents(MotusWord)[0].props().revealed).toBe(false);
    });

    it("should make interactive content disappear when real word is found (1 pts)", async () => {
        let wrapper = mount(Motus,{props : {word : "fleuri"}});
        await attempt(wrapper,"casque");
        await attempt(wrapper,"elargi");
        await attempt(wrapper,"fleuret");
        expect(wrapper.find("button").exists() && wrapper.find("button").isVisible()).toBe(true);
        expect(wrapper.find("input").exists() && wrapper.find("button").isVisible()).toBe(true);
        await attempt(wrapper,"fleuri");
        expect(wrapper.find("button").exists() && wrapper.find("button").isVisible()).toBe(false);
        expect(wrapper.find("input").exists() && wrapper.find("input").isVisible()).toBe(false);
    });
});

const testAttempt = async (motus,proposition,attemptNumber) => {
    await attempt(motus,proposition)
    let motusWords = motus.findAllComponents(MotusWord);
    expect(motusWords).toHaveLength(attemptNumber + 1);
    expect(motusWords[attemptNumber - 1].props().revealed).toBe(true);
    expect(motusWords[attemptNumber - 1].props().proposition).toBe(proposition);
};

const attempt = async (motus,attempt) => {
    await motus.get("input").setValue(attempt);
    await motus.get("button").trigger("click");
};