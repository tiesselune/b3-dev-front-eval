import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import LetterBox from '../LetterBox.vue'

describe('LetterBox', () => {
  it('should render a single letter in capital (1 pts)', () => {
    const wrapper = mount(LetterBox,{props : {letter : "b", revealed : true, status : "ok"}})
    expect(wrapper.text()).toBe("B");
    const wrapper2 = mount(LetterBox,{props : {letter : "c", revealed : true, status : "ok"}})
    expect(wrapper2.text()).toBe("C");
  });

  it('should always have class "box" on the letter element (1 pts)', () => {
    let wrapper = mount(LetterBox,{props : {letter : "E", revealed : true, status : "ok"}});
    expect(wrapper.findAll(".box")).toHaveLength(1);
    let wrapper2 = mount(LetterBox,{props : {letter : "E", revealed : false, status : "error"}});
    expect(wrapper2.findAll(".box")).toHaveLength(1);
  });

  it('should add appropriate class if revealed (1 pts)', () => {
    let wrapper = mount(LetterBox,{props : {letter : "E", revealed : true, status : "ok"}});
    expect(wrapper.find(".ok").exists()).toBe(true);
    expect(wrapper.find(".misplaced").exists()).toBe(false);
    expect(wrapper.find(".error").exists()).toBe(false);
    let wrapper2 = mount(LetterBox,{props : {letter : "G", revealed : true, status : "error"}});
    expect(wrapper2.find(".ok").exists()).toBe(false);
    expect(wrapper2.find(".misplaced").exists()).toBe(false);
    expect(wrapper2.find(".error").exists()).toBe(true);
    let wrapper3 = mount(LetterBox,{props : {letter : "H", revealed : true, status : "misplaced"}});
    expect(wrapper3.find(".ok").exists()).toBe(false);
    expect(wrapper3.find(".misplaced").exists()).toBe(true);
    expect(wrapper3.find(".error").exists()).toBe(false);
  });

  it('should not have status class if not revealed (1 pts)', () => {
    let wrapper4 = mount(LetterBox,{props : {letter : "I", revealed : false, status : "misplaced"}});
    expect(wrapper4.text()).not.toBe("");
    expect(wrapper4.find(".ok").exists()).toBe(false);
    expect(wrapper4.find(".misplaced").exists()).toBe(false);
    expect(wrapper4.find(".error").exists()).toBe(false);
    
    wrapper4.setProps({letter : "J", revealed : false, status : "error"})
    expect(wrapper4.text()).not.toBe("");
    expect(wrapper4.find(".ok").exists()).toBe(false);
    expect(wrapper4.find(".misplaced").exists()).toBe(false);
    expect(wrapper4.find(".error").exists()).toBe(false);
    wrapper4.setProps({letter : "K", revealed : false, status : "ok"})
    expect(wrapper4.text()).not.toBe("");
    expect(wrapper4.find(".ok").exists()).toBe(false);
    expect(wrapper4.find(".misplaced").exists()).toBe(false);
    expect(wrapper4.find(".error").exists()).toBe(false);
  });

  it('should display one letter only even if several are provided (0.5 pts)', () => {
    let wrapper = mount(LetterBox,{props : {letter : "Raquette", revealed : true, status : "ok"}});
    expect(wrapper.text()).toBe("R");
    let wrapper2 = mount(LetterBox,{props : {letter : "Youpi", revealed : true, status : "ok"}});
    expect(wrapper2.text()).toBe("Y");
  });

  it('should display a dot if no letter is provided (0.5 pts)', () => {
    let wrapper = mount(LetterBox,{props : {letter : "", revealed : true, status : "ok"}});
    expect(wrapper.text()).toBe(".");
  });
});
