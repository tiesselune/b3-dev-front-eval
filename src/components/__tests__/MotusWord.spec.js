import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import MotusWord from '../MotusWord.vue'
import LetterBox from "../LetterBox.vue";

describe('MotusWord', () => {
    it("should display as many LetterBoxes as the target word (1 pts)", () => {
        const wrapper = mount(MotusWord,{props : {word : "raton", proposition : "tenor",revealed : false}});
        expect(wrapper.findAllComponents(LetterBox)).toHaveLength(5);
        const wrapper2 = mount(MotusWord,{props : {word : "ynov", proposition : "manger",revealed : false}});
        expect(wrapper2.findAllComponents(LetterBox)).toHaveLength(4);
        const wrapper3 = mount(MotusWord,{props : {word : "devfront", proposition : "b3dev",revealed : false}});
        expect(wrapper3.findAllComponents(LetterBox)).toHaveLength(8);
    });
    it("should display letters from the proposition as successive LetterBoxes (2 pts)", () => {
        testWord("raton","tenor",false);
        testWord("casque","vision",false);
    });

    it("should truncate the proposition if it is longer than the target word (1 pts)", () => {
        testWord("ynov","frites",false);
    })

    it("should display only dots if the proposition is empty (1 pts)", () => {
        testWord("eval","",true);
    })
    it("should display only dots if the proposition is not provided at all (1 pts)", () => {
        testWord("eval",undefined,true);
    })
    it("should not reveal letters if revealed prop is not set to true (1 pts)", () => {
        const wrapper = mount(MotusWord,{props : {word : "raton", proposition : "tenor",revealed : false}});
        expect(wrapper.text()).not.toBe("");
        expect(wrapper.findAll(".ok")).toHaveLength(0);
        expect(wrapper.findAll(".error")).toHaveLength(0);
        expect(wrapper.findAll(".misplaced")).toHaveLength(0);
        const wrapper2 = mount(MotusWord,{props : {word : "tenor", proposition : "raton",revealed : false}});
        expect(wrapper2.text()).not.toBe("");
        expect(wrapper2.findAll(".ok")).toHaveLength(0);
        expect(wrapper2.findAll(".error")).toHaveLength(0);
        expect(wrapper2.findAll(".misplaced")).toHaveLength(0);
    })
    it("should reveal letters if revealed prop is set to true (1 pts)", () => {
        const wrapper = mount(MotusWord,{props : {word : "raton", proposition : "tenor",revealed : true}});
        expect(wrapper.findAll(".ok")).toHaveLength(1);
        expect(wrapper.findAll(".error")).toHaveLength(1);
        expect(wrapper.findAll(".misplaced")).toHaveLength(3);
        const wrapper2 = mount(MotusWord,{props : {word : "ynov", proposition : "inox",revealed : true}});
        expect(wrapper2.findAll(".ok")).toHaveLength(2);
        expect(wrapper2.findAll(".error")).toHaveLength(2);
        expect(wrapper2.findAll(".misplaced")).toHaveLength(0);
    })

});

const testWord = (word, proposition, revealed) => {
    const wrapper = mount(MotusWord,{props : {word, proposition, revealed}});
    const letters = wrapper.findAllComponents(LetterBox);
    expect(letters).toHaveLength(word.length);

    for(let i = 0; i < letters.length; i++){
        if(proposition && proposition.length > i){
            expect(letters[i].text()).toBe(proposition[i].toUpperCase());
        }
        else {
            expect(letters[i].text()).toBe(".");
        }
    }
}