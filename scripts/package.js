import { stdin, stdout } from "process";
import readline from "readline";
import archiver from "archiver";
import {createReadStream, createWriteStream} from "fs";
import { basename, } from "path";

const TARGET_FILES = ["src/components/LetterBox.vue","src/components/MotusWord.vue","src/components/Motus.vue"]

const red = "\x1b[31m";
const yellow = "\x1b[33m";
const green = "\x1b[32m";
const bold = "\x1b[1m";
const off = "\x1b[0m";

let prompt = (message) => {
    return new Promise((res,rej) => {
        const rl = readline.createInterface(stdin,stdout);
        rl.question(message,(answer) => {
            if(answer && answer.length > 0){
                rl.close();
                res(answer);
            }
            else {
                rl.close();
                rej("Veuillez taper une réponse.")
            }
        });
    })
};

const createArchive = (files,fileName) => {
    return new Promise((res,rej) => {
        const output = createWriteStream(`./${fileName}.zip`);
        const archive = archiver('zip', {
            zlib: { level: 9 }
        });
        archive.on("end",(e) => {
            res();
        });
        archive.on('error', function(err) {
            rej(err);
        });
        archive.pipe(output);
        for(const file of files){
            archive.append(createReadStream(file), {name : basename(file)});
        }
        archive.finalize();
    });
    
}

const main = async () => {
    const lastName = await prompt(`${bold}Votre nom de famille :${off} `);
    const firstName = await prompt(`${bold}Votre prénom :${off} `);
    const archName = `${lastName.toUpperCase()}_${firstName}`;
    try {
        await createArchive(TARGET_FILES,archName);
    }
    catch(e){
        console.error(`${bold}${red}Quelque chose s'est mal passé. Vérifiez que tous les fichiers sont présents et réessayez.${off}`,e);
    }
    
    console.log(`${bold}${green}Archive créée sous le nom ${archName}.zip${off}`);
    console.log(`Veuillez vérifier son contenu et ${bold}rendre ce fichier sur Moodle.${off}`);
}

main();