import { spawn } from "child_process";

const red = "\x1b[31m";
const yellow = "\x1b[33m";
const green = "\x1b[32m";
const bold = "\x1b[1m";
const off = "\x1b[0m";

const runTests = () => {
    return new Promise((res,rej) => {
        let testsProcess = spawn(/^win/.test(process.platform) ? 'npm.cmd' : "npm",["run","test:json"]);
        let result = "";
        testsProcess.stdout.on("data",(data) => {
            result += data;
        });
        testsProcess.on("exit",(code) => {
            result = result.split("\n").filter((e) => !e.startsWith(">")).join("\n");
            res(JSON.parse(result));
        });
    });
}

const getPoints = (testText) => {
    const pointsRegExp = /.*\(\s?(\d+\.?\d*)\s?pts\s?\)/gi;
    const res = pointsRegExp.exec(testText);
    if(res.length >=2){
        return parseFloat(res[1]);
    }
    return 0;
}

const main = async () => {
    const result = await runTests();
    let total = 0;
    let points = 0;
    let partial = false;
    let verbose = process.argv.length == 2;
    for(const group of result.testResults){
        if(group.message != ""){
            if(verbose){
                console.error(`${red}${bold}Une erreur empêche de lancer certains tests.${off} Veuillez résoudre le problème et relancer la commande.`);
                console.error("\n" + red +group.message + off + "\n");
            }
            else {
                console.error(`${yellow}${bold}Résultats partiels${off}`);
            }
            partial = true;
        }
        for(const test of group.assertionResults){
            const testPoints = getPoints(test.title);
            total += testPoints;
            if(test.status === "passed"){
                points += testPoints;
            }
        }
        
    }
    let coloredPoints = partial ? `${points} points` : `${points > total/2 ? green : red}${bold}${points} points${off}`;
    if(verbose){
        console.log(`Actuellement, vous validez ${coloredPoints} ${partial ? `${bold}${yellow}(Résulats partiels)${off}` : `sur ${total}.`}`)
        if(points == total){
            console.log(`${bold}${green}Félicitations, vous avez validé tous les tests!${off}`)
            console.log(`Vous pouvez lancer 'npm run package' et rendre le fichier sur Moodle.`);
        }
    }
    else {
        console.log(`${points > total/2 ? green : red}${bold}${points}/${total}${off}`)
    }
    if(partial){
        process.exit(1);
    }
};

main();