# Evaluation B3 Dev Front

Ce projet a pour but de vous faire développer 3 composants :
 - `<LetterBox>`
 - `<MotusWord>`
 - `<Motus>`

**Dans cet ordre**.

Chaque composant doit utiliser le précédent, mais les tests sont faits de manière à ce que vous puissiez passer au suivant sans que le précédent ne soit terminé. La seule condition pour valider les tests de `MotusWord` est que `LetterBox` ait les bons props, et idem pour `Motus` et `MotusWord`.

Des instructions se trouvent dans chaque fichier. Lisez-les attentivement, et lisez les résultats des tests échoués!

 - `LetterBox` affiche une boîte pour une lettre, sous un des 4 états : caché (fond bleu), erroné (fond gris), mal placé (fond jaune), bien placé (fond rouge)
 - `MotusWord` affiche un mot complet de la grille de Motus en fonction d'un mot et d'une tentative, dont les lettres peuvent être cachées (fond bleu) ou révélées (fonds gris, jaune et rouge)
 - `Motus` Affiche la liste des tentatives ainsi qu'un input et une ligne synchronisée avec l'input affichant le mot non-révélé jusqu'à sa validation. Lors de sa validation le mot est rajouté à la liste et révélé, et l'input vidé (voir captures plus bas)

⚠ Les classes CSS fournies dans `LetterBox.vue` doivent être utilisées pour faire passer les tests.

Le but de cet exercice est de valider le plus de tests possibles lorsqu'on lance `npm run test` ou `npm run test:ui` après avoir installé le projet avec `npm install`.

Le fichier `App.vue` contient des démonstrations avec lesquelles vous pouvez tester votre implémentation visuellement (`npm run dev`)

## Captures

### LetterBox

Quatre lettres dans leurs différents états:

```vue
  <LetterBox letter="y" status="ok" :revealed="true"></LetterBox>
  <LetterBox letter="n" status="misplaced" :revealed="true"></LetterBox>
  <LetterBox letter="o" status="error" :revealed="true"></LetterBox>
  <LetterBox letter="v" status="misplaced" :revealed="false"></LetterBox>
```

Rendu:

![LetterBox](./readme_files/LetterBox.png)

### MotusWord

```vue
<MotusWord word="raton" proposition="tenor" :revealed="false"></MotusWord>
```
![MotusWord](./readme_files/MotusWord.png)

```vue
<MotusWord word="raton" proposition="tenor" :revealed="true"></MotusWord>
```
![MotusWord](./readme_files/MotusWord2.png)


### Motus

```vue
<Motus word="danse"></Motus>
```

Initialement:  
![Motus1](./readme_files/Motus1.png)

En saisissant du texte dans le champ de saisie:  
![Motus2](./readme_files/Motus2.png)

Après avoir validé une tentative  
![Motus3](./readme_files/Motus3.png)

Après avoir validé plusieurs tentatives et avoir trouvé la solution :  

![Motus4](./readme_files/Motus4.png)

## Installation
```
npm install
```

### Compilation et lancement de la démo
```
npm run dev
```

### Lancement des tests
```
npm run test
```

ou avec l'UI web:

```
npm run test:ui
```

### Connaître votre note provisionnelle 

```
npm run grade
```

### Créer l'archive à rendre

A la fin de l'évaluation, créez l'archive de rendu grâce à:

```
npm run package
```
:warning: Vérifiez qu'elle contient bien les 3 fichiers source !

Vous pouvez ensuite rendre cette archive sur Moodle.
